import Head from "next/head";
import React from "react";

interface HeadTitleProps {
  title: string;
  description: string;
}

const HeadTitle = ({ title, description }: HeadTitleProps) => {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
      </Head>
    </div>
  );
};

export default HeadTitle;
