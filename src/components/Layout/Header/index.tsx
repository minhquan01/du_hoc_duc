import React from "react";

const Header = () => {
  return (
    <div
      style={{
        background: "linear-gradient(45deg, #7ea8f5, #144AAC)",
      }}
      className="h-20"
    >
      <div className="max-w-main mx-auto h-full flex items-center">Header</div>
    </div>
  );
};

export default Header;
