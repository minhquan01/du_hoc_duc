import React, { ReactNode } from "react";

interface LayoutProps {
  children: ReactNode;
}

const LayoutWithForm = ({ children }: LayoutProps) => {
    debugger
    return (
        <div className='flex flex-col md:flex-row gap-0.5 w-full'>
            <div className='flex-1 md:flex-[4] bg-white text-red-500'>
                <div>{children}</div>
            </div>
            <div className='flex-1 md:flex-[2] bg-slate-300'>XYZ</div>
        </div>
    );
};

export default LayoutWithForm;